function renderDSSV(dssv) {
  var contentHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    var item = dssv[i];
    contentHTML += ` 
      <tr>
          <td> ${item.ma} </td>
          <td> ${item.ten} </td>
          <td> ${item.email} </td>
          <td> 0 </td>
          <td> 
          <button onclick="xoaSv('${item.ma}')" class="btn btn-danger"> Xóa </button> 
          <button onclick="suaSv('${item.ma}')" class="btn btn-warning"> Sửa </button>
          </td>
          
      `;
  }

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;

  return {
    ma: maSv,
    ten: tenSv,
    email: email,
    matKhau: matKhau,
    toan: toan,
    ly: ly,
    hoa: hoa,
  };
}
