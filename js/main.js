//

var dssv = [];

const DSSV_LOCAL = "DSSV_LOCAL";

// lấy dữ liệu từ localstorage
var jsonData = localStorage.getItem(DSSV_LOCAL);
dssv = JSON.parse(jsonData);
renderDSSV(dssv);

function themSv() {
  // lấy  thông tin từ form

  dssv.push(sv);
  console.log("dssv: ", dssv);

  // convert to JSON
  var dataJson = JSON.stringify(dssv);
  // lưu vào localstorage
  localStorage.setItem(DSSV_LOCAL, dataJson);

  //   renderDSSV
  renderDSSV(dssv);
}

function xoaSv(id) {
  var viTri = -1;
  for (var i = 0; i < dssv.length; i++) {
    var item = dssv[i];
    if (item.ma == id) {
      viTri = i;
    }
  }
  console.log("viTri: ", viTri);
  console.log("id: ", id);

  if (viTri != -1) {
    dssv.splice(viTri, 1);
    // convert to JSON
    var dataJson = JSON.stringify(dssv);
    // lưu vào localstorage
    localStorage.setItem(DSSV_LOCAL, dataJson);
    renderDSSV(dssv);
  }
}

function suaSv(id) {
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });

  // show thông tin lên form

  document.getElementById("txtMaSV").value = dssv[viTri].ma;
  document.getElementById("txtTenSV").value = dssv[viTri].ten;
  document.getElementById("txtEmail").value = dssv[viTri].email;
  document.getElementById("txtPass").value = dssv[viTri].matKhau;
  document.getElementById("txtDiemToan").value = dssv[viTri].toan;
  document.getElementById("txtDiemLy").value = dssv[viTri].ly;
  document.getElementById("txtDiemHoa").value = dssv[viTri].hoa;
}

function capNhatSv() {
  var sv = layThongTinTuForm();

  var viTri = dssv.findIndex(function (item) {
    return item.ma == sv.ma;
  });
  console.log("viTri: ", viTri);
  dssv[viTri] = sv;
  // convert to JSON
  var dataJson = JSON.stringify(dssv);
  // lưu vào localstorage
  localStorage.setItem(DSSV_LOCAL, dataJson);
  renderDSSV(dssv);
}
